import React, { useState, useEffect } from 'react';

const Likes = () => {
    const [posts, setPosts] = useState([]);
    const [likes, setLikes] = useState(0);
  
    useEffect(() => {
      fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(json => setPosts(json))
    }, []);
  
    useEffect(() => {
      const curtidas = 
        posts.filter(post => post.like).length;
      setLikes(curtidas);
    }, [posts])
  
    const handleClick = (id) => {
      const novosPosts 
        = posts.map(post => {
          return post.id === id ? 
            {...post, like: !post.like} : post;
        });
      setPosts(novosPosts);
    }

    return (

        <div>
          <h1>Likes: {likes}</h1>
          <ul>
            {posts && posts.map(post => {
              return (
                <li
                  key={post.id}
                >{post.title}<br/>
                {post.like && 
                  <span>Curti!</span>
                }
                <button
                  onClick={() => handleClick(post.id)}
                >
                  Like
                </button>
                </li>
              );
            })}
          </ul>
        </div>
      );
}

export default Likes;
