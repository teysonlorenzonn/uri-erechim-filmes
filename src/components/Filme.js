import React, { useState } from 'react';
import '../css/Filme.css'



const Filme = () => {
    const [tipo, setTipo] = useState('');
    const [filmes, setFilmes] = useState([]);
    const [series, setSeries] = useState([]);
    const [tudo, setTudo] = useState([]);
    const [listLike, setlistLike] = useState([]);
    const [openFav, setOpenFav] = useState(true);
    const [openComp, setOpenComp] = useState(false);


    const buscarFilmes = () => {
        fetch(`http://www.omdbapi.com/?s=${tipo}&apikey=b13e933d`)
            .then(response => response.json())
            .then(json => {
                let seri = [];
                let film = [];
                json.Search.forEach(jso => {
                    if (jso.Type === 'movie'){
                        film.push(jso)
                    }else{
                        seri.push(jso)
                    }
                });
                
                const filS = seri.map(jso => {
                    return { ...jso, 'like': false};
                });
                const filF = film.map(jso => {
                    return { ...jso, 'like': false};
                });
                const fil = json.Search.map(jso => {
                    return { ...jso, 'like': false};
                });

                setFilmes(filF);
                setSeries(filS);
                setTudo(fil);
            })
            .catch(e => console.log(e))
    }

    const handleCompClick = () => {
        setOpenComp(true);
    }

    const handleFavClick = () => {
        setOpenComp(false);
        setOpenFav(true);
    }

    const handleClick = () => {
        setOpenComp(false);
        setOpenFav(false);
        buscarFilmes();
    }

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            setOpenComp(false);
            setOpenFav(false);
            buscarFilmes();
        }
    }

    const verifyLike = item => {
        for (let i = 0; i < listLike.length; i++) {
            if (item.imdbID === listLike[i].imdbID) {
                listLike.splice(i, 1);
                return false;
            }
        }
        
        item.like = true;
        return true;
    }

    const likeFilme = (filme) => {
        const newList = tudo.map(fi => {
            return fi.imdbID === filme.imdbID ? { ...fi, 'like': !fi.like } : fi
        });
        setTudo(newList);
        
        let like = listLike;
        verifyLike(filme) && like.push(filme);
        setlistLike(like);
    }


    const montagem = (tpe, msgSpan, label) => {
        return (

            <div className="row">
                { openComp && <h1 className='label-fil'> { label } </h1> }
                {tpe.length <= 0 ? <span className='span-fil' > {msgSpan} </span> : tpe.map(filme => {
                    return (
                        <div className="col-sm-2 col-md-4">
                            <div className="thumbnail border-block">
                                <div className="container-thumb">
                                    <img src={filme.Poster} className="img-fluid img-thumbnail img-fil" alt={filme.Title} />
                                    <div className="caption">
                                        <h3 className="title-style">{filme.Title}</h3>
                                        <ul key={filme.imdbID} >
                                            <li><b>Year:</b> {filme.Year}</li>
                                            <li><b>Type:</b> {filme.Type}</li>
                                            <li className={ filme.like ? 'like-fil-like' : 'like-fil' }  onClick={() => likeFilme(filme)}/>
                                        </ul>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })
                }
            </div>
        )
    }


    return (
        <div className='container'>
            <input className="form-control input-block"
                name='tipo'
                id='name_tipo'
                type='text'
                onChange={text => setTipo(text.target.value)}
                onKeyPress={event => handleKeyPress(event)}
            >
            </input>
            <button className="btn btn-primary" onClick={() => handleClick()}> Pesquisar </button>
            <button className="btn btn-primary" onClick={() => handleFavClick()}> Favoritos </button>
            <button className="btn btn-primary" onClick={() => handleCompClick()}> Compara </button>
            
            {openComp && montagem(filmes, 'Nenhuma filtro disponivel', 'Filmes')}
            {openComp && montagem(series, 'Nenhuma filtro disponivel', 'Series')}
            
            {!openComp && (openFav ? montagem(listLike, 'Nenhum Favorito', 'Likes') : montagem(tudo, 'Nenhum filme encontrado', ''))}

        </div>
    );

}

export default Filme;